package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Orders;
import com.example.demo.service.OrdersService;

@RestController
@CrossOrigin
@RequestMapping("api/orders")
public class OrdersController {
	@Autowired
	OrdersService service;

	@GetMapping(value = "/")
	public List<Orders> getAllOrders() {
		return service.getAllOrders();
	}

	@GetMapping(value = "ticker/{stockTicker}")
	public List<Orders> getOrderByTicker(@PathVariable("stockTicker") String ticker) {
		return service.getOrders(ticker);
	}

	@GetMapping(value = "id/{id}")
	public Orders getOrderById(@PathVariable("id") int id) {
		return service.getOrder(id);
	}

	@GetMapping(value = "buyorsell/{buyOrSell}")
	public List<Orders> getOrdersByBuyOrSell(@PathVariable("buyOrSell") String method) {
		return service.getOrdersByBuyOrSell(method);
	}

	@GetMapping(value = "status/{statusCode}")
	public List<Orders> getOrdersByStatusCode(@PathVariable("statusCode") int statusCode) {
		return service.getOrdersByStatusCode(statusCode);
	}

	@PostMapping(value = "/")
	public Orders addOrder(@RequestBody Orders orders) {
		return service.newOrder(orders);
	}

	@PutMapping(value = "/")
	public Orders editOrder(@RequestBody Orders orders) {
		return service.saveOrder(orders);
	}

	@DeleteMapping(value = "/{id}")
	public int deleteOrder(@PathVariable int id) {
		return service.deleteOrder(id);
	}
}
