package com.example.demo.entities;

import java.sql.Timestamp;

public class Orders {
	private int id;
	private String stockTicker;
	private double price;
	private int volume;
	private String buyOrSell;
	private int statusCode;
	private Timestamp orderTime = new Timestamp(19);

	public Orders() {

	}

	public Orders(int id, String stockTicker, double price, int volume, String buyOrSell, int statusCode,
			Timestamp orderTime) {
		super();
		this.id = id;
		this.stockTicker = stockTicker;
		this.price = price;
		this.volume = volume;
		this.buyOrSell = buyOrSell;
		this.statusCode = statusCode;
		this.orderTime = orderTime;
	}

	public Timestamp getOrderTime() {

		return (orderTime);

	}

//	public String getOrderTime() {
//		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:sss");
//		return dateFormat.format(orderTime);
//
//	}

	public void setOrderTime(Timestamp orderTime) {
		this.orderTime = orderTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStockTicker() {
		return stockTicker;
	}

	public void setStockTicker(String stockTicker) {
		this.stockTicker = stockTicker;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public String getBuyOrSell() {
		return buyOrSell;
	}

	public void setBuyOrSell(String buyOrSell) {
		this.buyOrSell = buyOrSell;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

}
