package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Orders;
import com.example.demo.exceptions.ResourceNotFoundException;

@Repository
public class MySQLOrdersRepository implements OrdersRepository {
	@Autowired
	JdbcTemplate template;

	@Override
	public List<Orders> getAllOrders() {
		String sql = "SELECT * FROM orders";

		return template.query(sql, new OrdersRowMapper());
	}

	@Override
	public List<Orders> getOrderByTicker(String ticker) {
		String sql = "SELECT * FROM orders WHERE stockTicker=?";
		return template.query(sql, new OrdersRowMapper(), ticker);
	}

	@Override
	public Orders getOrderById(int id) {
		try {
			String sql = "SELECT * FROM orders WHERE id=?";
			return template.queryForObject(sql, new OrdersRowMapper(), id);
		} catch(EmptyResultDataAccessException ex) {
			throw new ResourceNotFoundException();
		}
		}
	public List<Orders> getOrdersByBuyOrSell(String method) {
		String sql = "SELECT * FROM orders WHERE buyOrSell=?";
		return template.query(sql, new OrdersRowMapper(), method);
	}

	@Override
	public List<Orders> getOrdersByStatusCode(int statusCode) {
		String sql = "SELECT * FROM orders WHERE statusCode=?";
		return template.query(sql, new OrdersRowMapper(), statusCode);
	}

	@Override
	public Orders editOrder(Orders orders) {
		String sql = "UPDATE orders SET stockTicker = ?, Price = ?, Volume = ?, buyOrSell = ? ,statusCode= ?, ordertime= ?  "
				+ "WHERE id = ?";
		template.update(sql, orders.getStockTicker(), orders.getPrice(), orders.getVolume(), orders.getBuyOrSell(),
				orders.getStatusCode(), orders.getOrderTime(), orders.getId());
		return orders;
	}

	@Override
	public int deleteOrder(int id) {
		String sql = "DELETE FROM orders WHERE id = ?";
		template.update(sql, id);
		return id;
	}

	@Override
	public Orders addOrder(Orders orders) {

		String sql = "insert into orders(stockTicker,price,volume,buyOrSell,statusCode,ordertime) "
				+ "VALUES(?,?,?,?,?,?)";
		template.update(sql, orders.getStockTicker(), orders.getPrice(), orders.getVolume(), orders.getBuyOrSell(),
				orders.getStatusCode(), orders.getOrderTime());
		return orders;
	}

}

class OrdersRowMapper implements RowMapper<Orders> {
	@Override
	public Orders mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Orders(rs.getInt("id"), rs.getString("stockTicker"), rs.getDouble("price"), rs.getInt("volume"),
				rs.getString("buyOrSell"), rs.getInt("statusCode"), rs.getTimestamp("ordertime"));
	}

}
