package com.example.demo.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.entities.Orders;

@Component
public interface OrdersRepository {
	public List<Orders> getAllOrders();

	public List<Orders> getOrderByTicker(String ticker);

	public Orders getOrderById(int id);

	public Orders editOrder(Orders orders);
	
	public int deleteOrder(int id);

	public Orders addOrder(Orders orders);

	public List<Orders> getOrdersByBuyOrSell(String method);

	public List<Orders> getOrdersByStatusCode(int statusCode);
}
