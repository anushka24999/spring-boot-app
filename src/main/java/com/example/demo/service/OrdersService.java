package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Orders;
import com.example.demo.repository.OrdersRepository;

@Service
public class OrdersService {
	@Autowired
	private OrdersRepository repository;

	public List<Orders> getAllOrders() {

		return repository.getAllOrders();
	}

	public List<Orders> getOrders(String ticker) {
		return repository.getOrderByTicker(ticker);
	}

	public Orders getOrder(int id) {
		return repository.getOrderById(id);
	}
	public List<Orders> getOrdersByBuyOrSell(String method) {
		return repository.getOrdersByBuyOrSell(method);

	}

	public List<Orders> getOrdersByStatusCode(int statusCode) {
		return repository.getOrdersByStatusCode(statusCode);
	}

	public Orders saveOrder(Orders orders) {
		return repository.editOrder(orders);
	}

	public Orders newOrder(Orders orders) {
		return repository.addOrder(orders);
	}

	public int deleteOrder(int id) {
		return repository.deleteOrder(id);
	}
}
