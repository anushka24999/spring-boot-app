package com.example.demo.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Timestamp;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.example.demo.entities.Orders;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@ActiveProfiles("mysql")
@SpringBootTest
@AutoConfigureMockMvc
public class OrderControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testShouldReturnListSize3() throws Exception {
		// 1. setup stuff

		// 2. call method under test
		MvcResult mvcResult = this.mockMvc.perform(get("/api/orders/")).andDo(print()).andExpect(status().isOk())
				.andReturn();

		// 3. verify the results
		List<Orders> orders = new ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(),
				new TypeReference<List<Orders>>() {
				});

		assertThat(orders.size()).isGreaterThan(0);
	}

	@Test
	public void testFindByTicker() throws Exception {
		// 1. setup stuff
		String testTicker = "AMZN";
		// 2. call method under test
		MvcResult mvcResult = this.mockMvc.perform(get("/api/orders/ticker/" + testTicker)).andDo(print()).andExpect(status().isOk())
				.andReturn();

		// 3. verify the results
		List<Orders> orders = new ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(),
				new TypeReference<List<Orders>>() {
				});

		assertThat(orders.size()).isGreaterThan(0);
	}

	@Test
	public void testListByBuyOrSell() throws Exception {
		// 1. setup stuff
		String testbuyOrSell = "BUY";
		// 2. call method under test
		MvcResult mvcResult = this.mockMvc.perform(get("/api/orders/buyorsell/" + testbuyOrSell)).andDo(print()).andExpect(status().isOk())
				.andReturn();

		// 3. verify the results
		List<Orders> orders = new ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(),
				new TypeReference<List<Orders>>() {
				});

		assertThat(orders.size()).isGreaterThan(0);
	}

	@Test
	public void testListByStatusCode() throws Exception {
		// 1. setup stuff
		int teststatusCode = 0;
		// 2. call method under test
		MvcResult mvcResult = this.mockMvc.perform(get("/api/orders/status/" + teststatusCode)).andDo(print()).andExpect(status().isOk())
				.andReturn();

		// 3. verify the results
		List<Orders> orders = new ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(),
				new TypeReference<List<Orders>>() {
				});

		assertThat(orders.size()).isGreaterThan(0);
	}

	@Test
	public void testFindByIdSuccess() throws Exception {
		// 1. setup stuff
		int testId = 1;

		// 2. call method under test
		MvcResult mvcResult = this.mockMvc.perform(get("/api/orders/id/" + testId)).andDo(print())
				.andExpect(status().isOk()).andReturn();

		// 3. verify the results
		Orders orders = new ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(),
				new TypeReference<Orders>() {
				});

		assertThat(orders.getId()).isEqualTo(testId);
	}

	@Test
	public void testFindByIdFailure() throws Exception {
		// 1. setup stuff

		// 2. call method under test
		this.mockMvc.perform(get("/api/orders/id/99")).andDo(print()).andExpect(status().isNotFound());
	}

	@Test
	public void testCreateOrders() throws Exception {
		// 1. setup stuff
		String teststockTicker = "AMZN";
		double testprice = 3500;
		int testvolume = 10;
		String testbuyOrSell = "BUY";
		int teststatusCode = 0;
		Timestamp testorderTime = new Timestamp(19);

		Orders testOrders = new Orders();
		testOrders.setStockTicker(teststockTicker);
		testOrders.setPrice(testprice);
		testOrders.setVolume(testvolume);
		testOrders.setBuyOrSell(testbuyOrSell);
		testOrders.setStatusCode(teststatusCode);
		testOrders.setOrderTime(testorderTime);

		ObjectMapper mapper = new ObjectMapper();
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(testOrders);

		System.out.println(requestJson);

		// 2. call method under test
		MvcResult mvcResult = this.mockMvc
				.perform(post("/api/orders/").header("Content-Type", "application/json").content(requestJson))
				.andDo(print()).andExpect(status().isOk()).andReturn();

		// 3. verify the results
		Orders orders = new ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(),
				new TypeReference<Orders>() {
				});

		assertThat(orders.getStockTicker()).isEqualTo(teststockTicker);
		assertThat(orders.getPrice()).isEqualTo(testprice);
		assertThat(orders.getVolume()).isEqualTo(testvolume);
		assertThat(orders.getBuyOrSell()).isEqualTo(testbuyOrSell);
		assertThat(orders.getStatusCode()).isEqualTo(teststatusCode);
	}

	@Test
	public void testDeleteOrders() throws Exception {
		// 1. setup stuff
		int testId = 4;

		// 2. call method under test
		MvcResult mvcResult = this.mockMvc
				.perform(delete("/api/orders/" + testId).header("Content-Type", "application/json")).andDo(print())
				.andExpect(status().isOk()).andReturn();

		// 3. verify the results
		int resultId = new ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(),
				new TypeReference<Integer>() {
				});

		assertThat(testId).isEqualTo(resultId);

		// ensure this id is not found now
		this.mockMvc.perform(get("/api/orders/id/" + testId)).andDo(print()).andExpect(status().isNotFound());
	}

	@Test
	public void testEditShipper() throws Exception {
		// 1. setup stuff
		int testid = 1;
		String teststockTicker = "AMZN";
		double testprice = 3500;
		int testvolume = 10;
		String testbuyOrSell = "BUY";
		int teststatusCode = 3;
		Timestamp testorderTime = new Timestamp(19);

		Orders testOrders = new Orders();
		testOrders.setId(testid);
		testOrders.setStockTicker(teststockTicker);
		testOrders.setPrice(testprice);
		testOrders.setVolume(testvolume);
		testOrders.setBuyOrSell(testbuyOrSell);
		testOrders.setStatusCode(teststatusCode);
		testOrders.setOrderTime(testorderTime);

		ObjectMapper mapper = new ObjectMapper();
		ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson = ow.writeValueAsString(testOrders);

		System.out.println(requestJson);

		// 2. call method under test
		MvcResult mvcResult = this.mockMvc.perform(put("/api/orders/")
										  			.header("Content-Type", "application/json")
										  			.content(requestJson))
								.andDo(print()).andExpect(status().isOk()).andReturn();
		
		// 3. verify the results
		Orders orders = new ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(),
				new TypeReference<Orders>() {
				});

		assertThat(orders.getStockTicker()).isEqualTo(teststockTicker);
		// assertThat(orders.getPrice()).isEqualTo(testprice);
		assertThat(orders.getVolume()).isEqualTo(testvolume);
		assertThat(orders.getBuyOrSell()).isEqualTo(testbuyOrSell);
		// assertThat(orders.getStatusCode()).isEqualTo(teststatusCode);
		
		// verify the edit took place
		mvcResult = this.mockMvc.perform(get("/api/orders/id/" +  testid))
	              .andDo(print())
	              .andExpect(status().isOk())
	              .andReturn();

		orders = new ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(),
				new TypeReference<Orders>() {
				});

		assertThat(orders.getId()).isEqualTo(testid);
		assertThat(orders.getStockTicker()).isEqualTo(teststockTicker);
		assertThat(orders.getPrice()).isEqualTo(testprice);
		assertThat(orders.getVolume()).isEqualTo(testvolume);
		assertThat(orders.getBuyOrSell()).isEqualTo(testbuyOrSell);
		assertThat(orders.getStatusCode()).isEqualTo(teststatusCode);
	}
}
