/**
 * 
 */
package com.example.demo.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.example.demo.entities.Orders;

@ActiveProfiles("h2")
@SpringBootTest
public class MySQLOrderRepositoryTest {

	@Autowired
	MySQLOrdersRepository mySQLOrdersRepository;
		
	@Test
	public void testGetAllOrders() {
		// 1. Any setup stuff
		
		// 2. Call the method under test
		List<Orders> returnedList = mySQLOrdersRepository.getAllOrders();
		
		// 3 Verify the results
		assertThat(returnedList).isNotEmpty();
		
		for(Orders orders: returnedList) {
			System.out.println("Stock Ticker: " + orders.getStockTicker());
		}
	}
}
