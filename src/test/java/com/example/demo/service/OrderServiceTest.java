/**
 * A class for mocking tests of the OrdersService
 * 
 * Note the extra comments here are for training purposes ONLY
 * 
 * In production these comments would not be included.
 */
package com.example.demo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import com.example.demo.entities.Orders;
import com.example.demo.exceptions.ResourceNotFoundException;
import com.example.demo.repository.OrdersRepository;

@ActiveProfiles("h2")
@SpringBootTest
public class OrderServiceTest {

	// Get a "real" ordersService from the spring container
	@Autowired
	OrdersService ordersService;

	// Get a "mock" or "fake" orders repository
	// from the spring container
	@MockBean
	OrdersRepository ordersRepository;

	@Test
	public void testGetOrders() {
		// 1. Setup stuff
		// create a orders record for testing
		int testid=99;
		String teststockTicker = "AMZN";
		double testprice=3500;
		int testvolume =10;
		String testbuyOrSell = "BUY";
		int teststatusCode = 0;
		Timestamp testorderTime = new Timestamp(19);
		Orders testOrders = new Orders();
		testOrders.setId(testid);
		testOrders.setStockTicker(teststockTicker);
		testOrders.setPrice(testprice);
		testOrders.setVolume(testvolume);
		testOrders.setBuyOrSell(testbuyOrSell);
		testOrders.setStatusCode(teststatusCode);
		testOrders.setOrderTime(testorderTime);

		// tell the mock object what to do when
		// its getOrdersById method is called
		when(ordersRepository.getOrderById(testOrders.getId())).thenReturn(testOrders);

		// 2. call class under test
		// call the getOrders method on the service, this will call
		// the getOrdersById method on the the mock repository
		// the mock repository returns the testOrders
		// the service should return the same testOrders here
		// we verify this happens, so we know the service is behaving as expected
		Orders returnedOrders = ordersService.getOrder(testid);

		// 3. verify response
		assertThat(returnedOrders).isEqualTo(testOrders);
	}

	// Test for a "NotFound scenario"
	// NOTE: this test is unnecessary for this particular class.
	// It's only here to demonstrate how to generate and test for
	// an exception in a test that uses mocking.
	@Test
	public void testGetOrdersNotFound() {
		// 1. Setup stuff
		int testid = 99;

		// tell the mock object what to do when
		// its getOrdersById method is called
		// in this case it needs to throw an exception
		when(ordersRepository.getOrderById(testid)).thenThrow(new ResourceNotFoundException());

		// 2. call class under test & 3. verify the results
		// call the getOrders method on the service, this will call
		// the getOrdersById method on the the mock repository
		// the mock repository throws an exception
		// the service doesn't catch that exception
		Assertions.assertThrows(ResourceNotFoundException.class, () -> {
			ordersService.getOrder(testid);
		});
	}
}
